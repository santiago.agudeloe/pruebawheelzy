﻿using System.Collections.Generic;
using System.Linq;

namespace PruebaWheelzy.Models
{
    public class Estado
    {
        public int IdEstado { get; set; }

        public string NombreEstado { get; set; } = null!;

        public DateTime FechaEstado { get; set; }

        public string AutorCambio { get; set; } = null!;

        public virtual ICollection<Coche> Coches { get; set; } = new List<Coche>();
    }
}
