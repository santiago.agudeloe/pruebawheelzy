﻿using System;
using System.Collections.Generic;

namespace PruebaWheelzy.Models.DB;

public partial class Comprador
{
    public int IdComprador { get; set; }

    public string Nombre { get; set; } = null!;

    public string Cotizacion { get; set; } = null!;

    public virtual ICollection<Coche> Coches { get; set; } = new List<Coche>();
}
