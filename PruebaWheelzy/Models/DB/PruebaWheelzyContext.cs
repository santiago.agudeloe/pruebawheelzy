﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace PruebaWheelzy.Models.DB;

public partial class PruebaWheelzyContext : DbContext
{
    public PruebaWheelzyContext()
    {
    }

    public PruebaWheelzyContext(DbContextOptions<PruebaWheelzyContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Coche> Coches { get; set; }

    public virtual DbSet<Comprador> Compradors { get; set; }

    public virtual DbSet<Estado> Estados { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=(local);DataBase=PruebaWheelzy;Trusted_Connection=true;TrustServerCertificate=true");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Coche>(entity =>
        {
            entity.HasKey(e => e.IdCoche).HasName("PK__Coche__3635C7C464340BA8");

            entity.ToTable("Coche");

            entity.Property(e => e.IdCoche).HasColumnName("idCoche");
            entity.Property(e => e.AnioCoche)
                .HasMaxLength(50)
                .HasColumnName("anio_coche");
            entity.Property(e => e.CodigoPostal)
                .HasMaxLength(30)
                .HasColumnName("codigoPostal");
            entity.Property(e => e.IdComprador).HasColumnName("idComprador");
            entity.Property(e => e.IdEstado).HasColumnName("idEstado");
            entity.Property(e => e.Marca)
                .HasMaxLength(50)
                .HasColumnName("marca");
            entity.Property(e => e.Modelo)
                .HasMaxLength(50)
                .HasColumnName("modelo");
            entity.Property(e => e.Submodelo)
                .HasMaxLength(50)
                .HasColumnName("submodelo");

            entity.HasOne(d => d.IdCompradorNavigation).WithMany(p => p.Coches)
                .HasForeignKey(d => d.IdComprador)
                .HasConstraintName("fk_comprador");

            entity.HasOne(d => d.IdEstadoNavigation).WithMany(p => p.Coches)
                .HasForeignKey(d => d.IdEstado)
                .HasConstraintName("fk_estado");
        });

        modelBuilder.Entity<Comprador>(entity =>
        {
            entity.HasKey(e => e.IdComprador).HasName("PK__Comprado__AA3C999F5B5B6580");

            entity.ToTable("Comprador");

            entity.Property(e => e.IdComprador).HasColumnName("idComprador");
            entity.Property(e => e.Cotizacion)
                .HasMaxLength(50)
                .HasColumnName("cotizacion");
            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .HasColumnName("nombre");
        });

        modelBuilder.Entity<Estado>(entity =>
        {
            entity.HasKey(e => e.IdEstado).HasName("PK__Estado__62EA894ACD524D05");

            entity.ToTable("Estado");

            entity.Property(e => e.IdEstado).HasColumnName("idEstado");
            entity.Property(e => e.AutorCambio)
                .HasMaxLength(50)
                .HasColumnName("autor_cambio");
            entity.Property(e => e.FechaEstado)
                .HasColumnType("datetime")
                .HasColumnName("fechaEstado");
            entity.Property(e => e.NombreEstado)
                .HasMaxLength(50)
                .HasColumnName("nombreEstado");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
