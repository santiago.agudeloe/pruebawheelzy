﻿using System;
using System.Collections.Generic;

namespace PruebaWheelzy.Models.DB;

public partial class Coche
{
    public int IdCoche { get; set; }

    public string AnioCoche { get; set; } = null!;

    public string Marca { get; set; } = null!;

    public string Modelo { get; set; } = null!;

    public string Submodelo { get; set; } = null!;

    public string CodigoPostal { get; set; } = null!;

    public int? IdComprador { get; set; }

    public int? IdEstado { get; set; }

    public virtual Comprador? IdCompradorNavigation { get; set; }

    public virtual Estado? IdEstadoNavigation { get; set; }
}
