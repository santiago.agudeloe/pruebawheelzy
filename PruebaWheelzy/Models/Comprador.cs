﻿using System.Collections.Generic;
using System.Linq;

namespace PruebaWheelzy.Models

{
    public class Comprador
    {
        public int IdComprador { get; set; }

        public string Nombre { get; set; } = null!;

        public string Cotizacion { get; set; } = null!;

        public virtual ICollection<Coche> Coches { get; set; } = new List<Coche>();
    }
}
