﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PruebaWheelzy.Models;

namespace PruebaWheelzy.Controllers
{
    public class CocheController : Controller
    {
        // GET: CocheController
        public ActionResult Index()
        {
            List<Coche> lst = new List<Coche> ();
            using (var db = new Models.DB.PruebaWheelzyContext())
            {
                lst = (from d in db.Coches
                      select new Coche
                      {
                          IdCoche = d.IdCoche,
                          AnioCoche = d.AnioCoche,
                          Marca = d.Marca,
                          Modelo = d.Modelo,
                          Submodelo = d.Submodelo,
                          CodigoPostal = d.CodigoPostal,
                          IdComprador = d.IdComprador,
                          IdEstado = d.IdEstado,

                      }).ToList ();
            }
            return View(lst);
        }

       
        // GET: CocheController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CocheController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CocheController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CocheController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CocheController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CocheController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CocheController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
